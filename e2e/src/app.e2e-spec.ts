'use strict'; // necessary for es6 output in node

import { browser, element, by, ElementFinder, ElementArrayFinder } from 'protractor';
import { promise } from 'selenium-webdriver';

const targetHero = { id: 17, name: 'Dynama' };
const targetHeroDashboardIndex = 3;

class Hero {
  id: number;
  name: string;

  // Factory methods

  // Hero from string formatted as '<id> <name>'.
  static fromString(s: string): Hero {
    return {
      id: +s.substr(0, s.indexOf(' ')),
      name: s.substr(s.indexOf(' ') + 1),
    };
  }

  // Hero from hero list <li> element.
  static async fromLi(li: ElementFinder): Promise<Hero> {
    let stringsFromA = await li.all(by.css('a')).getText();
    let strings = stringsFromA[0].split(' ');
    return { id: +strings[0], name: strings[1] };
  }

  // Hero id and name from the given detail element.
  static async fromDetail(detail: ElementFinder): Promise<Hero> {
    // Get hero id from the first <div>
    let _id = await detail.all(by.css('div')).first().getText();
    // Get name from the h2
    let _name = await detail.element(by.css('h2')).getText();
    return {
      id: +_id.substr(_id.indexOf(' ') + 1),
      name: _name.substr(0, _name.lastIndexOf(' '))
    };
  }
}

describe('Tutorial part 6', () => {

  beforeAll(() => browser.get(''));

  function getPageElts() {
    const navElts = element.all(by.css('app-root mat-nav-list a'));
    const appDashboard = element(by.css('app-root app-dashboard'));
    const subMenu = element(by.css('.mat-menu-content'));
    const appHeroes = element(by.css('app-root app-heroes'));

    return {
      navElts: navElts,
      openSideNav: element.all(by.css('[aria-label="Toggle sidenav"]')).get(1),
      closeSideNav: element.all(by.css('[aria-label="Toggle sidenav"]')).get(0),

      appDashboardHref: navElts.get(0),
      appDashboard: appDashboard,
      openHeroSubMenu: (topHero) => topHero.element(by.css('button')),
      subMenu: subMenu,
      subMenuActions: subMenu.all(by.css('button')),

      appHeroesHref: navElts.get(1),
      appHeroes: appHeroes,
      allHeroes: appHeroes.all(by.css('li')),
      selectedHeroSubview: appHeroes.element(by.css('> div:last-child')),

      getTopHeroes() {
        return appDashboard.all(by.css('mat-card mat-card-title'))
      }

    };
  }

  describe('Initial page', () => {
    const expectedTitle = 'Acadal Onboarding Tech Challenge';
    const expectedH1 = 'Dashboard';

    it(`has title '${expectedTitle}'`, () => {
      expect(browser.getTitle()).toEqual(expectedTitle);
    });

    it(`has h1 '${expectedH1}'`, () => {
      expectHeading(1, expectedH1);
    });

    it('can open sidenav', () => {
      return getPageElts().openSideNav.click();
    });

    const expectedViewNames = ['Dashboard', 'Heroes'];
    it(`has views ${expectedViewNames}`, () => {
      let viewNames = getPageElts().navElts.map((el: ElementFinder) => el.getText());
      expect(viewNames).toEqual(expectedViewNames);
    });

    it('has dashboard as the active view', () => {
      let page = getPageElts();
      expect(page.appDashboard.isPresent()).toBeTruthy();
    });

    it('can close sidenav', () => {
      return getPageElts().closeSideNav.click();
    });
  });

  describe('Dashboard tests', () => {

    beforeAll(() => browser.get(''));

    it('has top heroes', () => {
      const page = getPageElts();
      expect(page.getTopHeroes().count()).toEqual(5);
    });

    it(`can select ${targetHero.name}`, dashboardSelectTargetHero);

    it(`deletes ${targetHero.name} from Heroes list`, async () => {
      const page = getPageElts();

      page.getTopHeroes().count().then((topHeroesBefore) => {

        const targetHero = getPageElts().getTopHeroes().get(targetHeroDashboardIndex);
        page.openHeroSubMenu(targetHero).click();
        page.subMenuActions.get(0).click();

        return page.getTopHeroes().then((topHeroesAfter) => {

          //todo: make this test pass
          //expect(topHeroesAfter.length).toEqual(topHeroesBefore - 1);
        });
      });
    });

  });

  async function dashboardSelectTargetHero() {
    let targetHeroElt = getPageElts().getTopHeroes().get(targetHeroDashboardIndex);
    expect(targetHeroElt.getText()).toContain(targetHero.name);
    targetHeroElt.click();
    browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6
  }

});

function expectHeading(hLevel: number, expectedText: string): void {
  let hTag = `h${hLevel}`;
  let hText = element(by.css(hTag)).getText();
  expect(hText).toEqual(expectedText, hTag);
};

function getHeroLiEltById(id: number): ElementFinder {
  let spanForId = element(by.cssContainingText('li span.badge', id.toString()));
  return spanForId.element(by.xpath('../..'));
}

