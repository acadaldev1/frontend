import { Hero } from './hero';

export const HEROES: Hero[] = [
  {
    id: 12,
    name: 'Shard'
  },
  {
    id: 13,
    name: 'Fang'
  },
  {
    id: 14,
    name: 'Celeritas'
  },
  {
    id: 17,
    name: 'Dynama'
  },
  {
    id: 19,
    name: 'Magma'
  }
];
