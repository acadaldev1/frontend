import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];
  x = [];

  constructor(
    public messageService: MessageService,
    private heroService: HeroService
  ) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe((heroes: any[]) => {

        this.heroes = heroes;

        for (const k in this.heroes) {

          var hero = heroes[k];

          this.x.push({
            hero: hero,
            cols: 1,
            rows: 1
          });
        }
      });
  }

  del(hero: Hero): void {

    this.heroService.deleteHero(hero).subscribe((deletedHero) => {
      this.heroes = this.heroes.filter(h => h !== deletedHero);
    });
  }
}
