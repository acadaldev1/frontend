import { HEROES } from './mock-heroes';
import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = HEROES;
    return { heroes };
  }
}
