import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-comp-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  @Input()
  cards: any[] = null;


  @Output()
  action = new EventEmitter();
}
