# Acadal Onboarding Tech Challenge - Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.

## Primary task

The DELETE HERO dashboard card menu button does nothing. Make it work, so that clicking the menu item deletes that hero!

##Secondary tasks

Refactor/simplify/rename/move the code as you see fit, to improve the following:
 - code readability
 - [code duplication](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)
 - [code cohesion](https://en.wikipedia.org/wiki/Cohesion_(computer_science))
 - adherence to the [single responsibility principle](https://en.wikipedia.org/wiki/Single_responsibility_principle)
 - adherence to the [open/closed principle](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle)
 - anything else you would like to improve.
 
Create a branch named YOUR_INITIALS-improvements, and make a pull request once you are done.

## Requirements
1. [Node.js and npm](https://nodejs.org/en/download/)
2. [Angular CLI] (https://github.com/angular/angular-cli) installed globally: `npm install -g @angular/cli`

## Development server

Run `ng serve --aot` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build --aot` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Using Docker images

### Setup
Docker is required in order to use Docker images. The app is used the Docker configs in the ./docker/ folder for dev/prod builds respectively. The Docker file is 
used to build this web app as a node app served from nginx.

### Build Docker image

*dev*: Run `docker build -t acadal/onboarding-frontend:dev . -f ./docker/Dockerfile.dev`
*prod*: Run `docker build -t acadal/onboarding-frontend:prod . -f ./docker/Dockerfile.prod`

### Run Docker container

Run `docker run -p 80:80 acadal/onboarding-frontend:dev`

Web app will then be available on http://localhost

### Docker troubleshooting

Getting an error when trying to run container?
Try restarting Docker.

Need to delete all images / containers?
Run `docker stop $(docker ps -a -q)` to stop all locally running containers.
Run `docker rm $(docker ps -a -q)` to delete all locally present containers.
Run `docker rmi $(docker images -a -q)` to delete all locally present images.

## General troubleshooting

Ng build does not work?
Run `rm -rf node_modules` to force flush locally cached npm packages.
Run `rm package-lock.json` to force usage of current package.json, instead of the locked one.
Run `npm install` to re-install npm packages.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
